# sboathud
HUD mod for boat racing.

### Features
- Displays a HUD with various data relevent for boat racing
- Write the data each tick to a .csv file
- Comparison with a previous run, using a delta file created with [delta_create.py](https://gitlab.com/5h1zuy4/tutils/-/blob/master/delta_create.py)
- Throttle/steering trace
- G meter
- Walltap/blockstop counter
- Compact mode HUD

### Dependencies
- [Fabric Loader](https://fabricmc.net/use/installer/) (>=0.15.11)
- [Fabric API](https://github.com/FabricMC/fabric/releases) (>=0.100.1)
- [Cloth Config API](https://modrinth.com/mod/cloth-config/versions) (>=15.0.127)
- [Mod Menu](https://github.com/TerraformersMC/ModMenu/releases) (>=11.0.0)
