package shizuya.sboathud;

import java.io.File;

import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.ConfigData;
import net.fabricmc.loader.api.FabricLoader;

@Config(name = "sboathud")
public class ModConfig implements ConfigData {
    
    public boolean hudEnabled = true;
    public boolean extendedHud = true;
    public boolean hideBars = true;
    public int yOffset = 36;
    public BarType barType = BarType.MIXED;
    public SpeedUnit speedUnit = SpeedUnit.MS;
    public AccelerationUnit accelerationUnit = AccelerationUnit.MSS;
    public boolean telemetryEnabled = false;
    public boolean deltaEnabled = false;
    public String deltaFile = "delta_file.cf";
    public boolean lapTimeStats = false;
    public LapTimeUnit lapTimeUnit = LapTimeUnit.MS;
    public boolean cringeCount = false;

    @Override
    public void validatePostLoad() {
        File telemetryDirectory = new File(getTelemetryDirectory());
        if (!telemetryDirectory.exists()) telemetryDirectory.mkdirs();
        File deltaDirectory = new File(getDeltaDirectory());
        if (!deltaDirectory.exists()) deltaDirectory.mkdirs();
    }

    public String getConfigDirectory() {
        return FabricLoader.getInstance().getConfigDir().toString() + File.separator;
    }

    public String getTelemetryDirectory() {
        return getConfigDirectory() + "sboathud" + File.separator + "telemetry" + File.separator;
    }

    public String getDeltaDirectory() {
        return getConfigDirectory() + "sboathud" + File.separator + "deltas" + File.separator;
    }

    public enum BarType {
        PACKED, MIXED, BLUE
    }

    public enum SpeedUnit {
        MS, KMH, MPH
    }

    public enum AccelerationUnit {
        MSS, G
    }

    public enum LapTimeUnit {
        MS, S
    }
}
