package shizuya.sboathud;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.PlayerListEntry;
import net.minecraft.entity.vehicle.BoatEntity;
import net.minecraft.util.math.Vec3d;

public class BoatData {

    private static final MinecraftClient CLIENT = MinecraftClient.getInstance();
    private static final int TICKRATE = 20;
    
    public final String name;
    private final PlayerListEntry listEntry;

    public int ping;
    public int fps;

    public double x;
    public double z;
    public double y;
    public double speed; // m/s
    public double lonAcc; // m/s²
    public double latAcc; // m/s²
    public double slipAngle; // °
    public double rotSpeed; // °/s
    public double steering;
    public double throttle;
    public double dirFacing; // °
    public double dirMoving; // °

    public double xLast;
    public double zLast;
    public double speedLast; // m/s
    private double dirFacingLast; // °
    private double dirMovingLast; // °

    public double friction;
    public int walltaps;
    public int blockstops;
    public Vec3d velocityLast;

    public Delta delta;
    public Telemetry telemetry;

    public Deque<Double> steeringTrace = new ArrayDeque<Double>(Collections.nCopies(40, 0d));
    public Deque<Double> throttleTrace = new ArrayDeque<Double>(Collections.nCopies(40, 0d));

    public BoatData() {
        this.name = Sboathud.client.player.getNameForScoreboard();
        this.listEntry = Sboathud.client.getNetworkHandler().getPlayerListEntry(Sboathud.client.player.getUuid());
        if (Sboathud.config.deltaEnabled) {
            this.delta = new Delta(Sboathud.config.deltaFile);
        }
        if (Sboathud.config.telemetryEnabled) {
            this.telemetry = new Telemetry();
        }
    }

    public void update() {
        BoatEntity boat = (BoatEntity) Sboathud.client.player.getVehicle();
        Vec3d velocity = boat.getVelocity().multiply(1, 0, 1); // Ignore vertical speed
        this.ping = this.listEntry.getLatency();

        this.updateLast();

        this.x = boat.getX();
        this.z = boat.getZ();
        this.y = boat.getY();
        this.speed = velocity.length() * TICKRATE;
        this.dirFacing = boat.getYaw();
        this.rotSpeed = (this.dirFacing - this.dirFacingLast) * TICKRATE;
        this.dirMoving = Math.toDegrees(Math.atan2(-velocity.getX(), velocity.getZ()));
        this.slipAngle = this.speed == 0 ? 0 : normaliseAngle(this.dirFacing - this.dirMoving);
        this.lonAcc = (this.speed - this.speedLast) * TICKRATE;
        this.latAcc = Math.sin(Math.toRadians(this.dirMoving - this.dirMovingLast) / 2) * this.speedLast * 2 * TICKRATE;
        this.steering = (CLIENT.options.leftKey.isPressed() ? -1d : 0) + (CLIENT.options.rightKey.isPressed() ? 1d : 0);
        this.throttle = (CLIENT.options.forwardKey.isPressed() ? 1d : 0) + (CLIENT.options.backKey.isPressed() ? -0.125 : 0);
        this.updateTrace();

        if (Sboathud.config.deltaEnabled && this.delta.checkpointCount > 0) {
            this.delta.passCheckpoint();
        }
        if (Sboathud.config.telemetryEnabled) {
            this.telemetry.write();
        }

        // Cringe detection
        if (velocityLast != null) {
            if (velocity.x == 0 && Math.abs(velocityLast.x) > 16.0 / TICKRATE / TICKRATE * 2) this.walltaps++;
            if (velocity.z == 0 && Math.abs(velocityLast.z) > 16.0 / TICKRATE / TICKRATE * 2) this.walltaps++;
        }
        this.friction = 1 - boat.getNearbySlipperiness();
        if (this.speed > 16.0 / this.friction / TICKRATE * 2) this.blockstops++;
        this.velocityLast = velocity;
    }

    private void updateLast() {
        this.xLast = this.x;
        this.zLast = this.z;
        this.speedLast = this.speed;
        this.dirFacingLast = this.dirFacing;
        this.dirMovingLast = this.dirMoving;
    }

    private static double normaliseAngle(double angle) {
        angle = (angle % 360 + 360) % 360;
        return angle > 180 ? angle - 360 : angle;
    }

    private void updateTrace() {
        this.steeringTrace.removeFirst();
        this.steeringTrace.addLast(this.steering);
        this.throttleTrace.removeFirst();
        this.throttleTrace.addLast(this.throttle);
    }
}
