package shizuya.sboathud.mixin;

import shizuya.sboathud.Sboathud;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin {
    @Shadow private static int currentFps;

    @Inject(method = "render", at = @At("TAIL"))
    private void getCurrentFPS(boolean tick, CallbackInfo ci) {
        if (tick && Sboathud.ridingBoat) Sboathud.boatData.fps = currentFps;
    }
}
