package shizuya.sboathud.mixin;

import shizuya.sboathud.Sboathud;
import shizuya.sboathud.HudRenderer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.render.RenderTickCounter;
import net.minecraft.client.gui.DrawContext;

@Mixin(InGameHud.class)
public abstract class InGameHudMixin {
    @Inject(
        method = "render",
        at = @At("HEAD")
    )
    private void render(DrawContext context, RenderTickCounter tickCounter, CallbackInfo info) {
        if (Sboathud.config.hudEnabled && Sboathud.ridingBoat && !(Sboathud.client.currentScreen instanceof ChatScreen)) {
            HudRenderer.get().render(context);
        }
    }

    @Inject(
            method = "renderStatusBars",
            at = @At("HEAD"),
            cancellable = true
    )
    private void renderStatusBars(DrawContext context, CallbackInfo ci) {
        if (!(Sboathud.config.hideBars && Sboathud.config.hudEnabled && Sboathud.ridingBoat && !(Sboathud.client.currentScreen instanceof ChatScreen))) return;
        ci.cancel();
    }

    @Inject(
            method = "renderExperienceBar",
            at = @At("HEAD"),
            cancellable = true
    )
    private void renderExperienceBar(DrawContext context, int x, CallbackInfo ci) {
        if (!(Sboathud.config.hideBars && Sboathud.config.hudEnabled && Sboathud.ridingBoat && !(Sboathud.client.currentScreen instanceof ChatScreen))) return;
        ci.cancel();
    }

    @Inject(
            method = "renderExperienceLevel",
            at = @At("HEAD"),
            cancellable = true
    )
    private void renderExperienceLevel(DrawContext context, RenderTickCounter tickCounter, CallbackInfo ci) {
        if (!(Sboathud.config.hideBars && Sboathud.config.hudEnabled && Sboathud.ridingBoat && !(Sboathud.client.currentScreen instanceof ChatScreen))) return;
        ci.cancel();
    }

    @Inject(
            method = "renderHotbar",
            at = @At("HEAD"),
            cancellable = true
    )
    private void renderHotbar(DrawContext context, RenderTickCounter tickCounter, CallbackInfo ci) {
        if (!(Sboathud.config.hideBars && Sboathud.config.hudEnabled && Sboathud.ridingBoat && !(Sboathud.client.currentScreen instanceof ChatScreen))) return;
        ci.cancel();
    }
}
