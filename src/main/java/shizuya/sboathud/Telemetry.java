package shizuya.sboathud;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class Telemetry {
    String fileName;

    public Telemetry() {
        this.fileName = Sboathud.config.getTelemetryDirectory() + DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss").format(LocalDateTime.now()) + ".csv";
        try {
            FileWriter fw = new FileWriter(this.fileName);
            fw.write("speed,slipAngle,x,z,y,directionFacing,steering,throttle\n");
            fw.close();
        } catch (IOException e) {
        }
    }
    
    public void write() {
        try {
            FileWriter fw = new FileWriter(this.fileName, true);
            fw.write(String.format(Locale.US, "%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.4f,%.4f\n", 
                Sboathud.boatData.speed, Sboathud.boatData.slipAngle, Sboathud.boatData.x, Sboathud.boatData.z, Sboathud.boatData.y, 
                Sboathud.boatData.dirFacing, Sboathud.boatData.steering, Sboathud.boatData.throttle));
            fw.close();
        } catch (IOException e) {
        }
    }
}
