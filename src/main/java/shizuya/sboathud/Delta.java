package shizuya.sboathud;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Delta {
    public String fileName;
    public ArrayList<Double[]> data = new ArrayList<Double[]>();
    public int checkpointCount;
    public int currentCheckpoint;
    public double timeDiff;
    public double speedDiff;

    private double time;
    private double startTime;

    public double lastLap;
    public double averageLap;
    public double bestLap;
    public int lapCount;

    public Delta(String fileName) {
        this.fileName = Sboathud.config.deltaFile;
        try {
            BufferedReader br = new BufferedReader(new FileReader(Sboathud.config.getDeltaDirectory() + this.fileName));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                if (line.contains("time")) continue;
                String[] s = line.split(",");
                if (s.length < 5) continue;
                Double[] point = new Double[5];
                for (int i = 0; i < 5; i++) {
                    point[i] = Double.parseDouble(s[i]);
                }
                data.add(point);
            }
            br.close();
        }
        catch (Exception e) {
        }
        this.checkpointCount = data.size();
    }

    public void passCheckpoint() {
        for (double distance = calculateDistance(Sboathud.boatData.x, Sboathud.boatData.z); distance > data.get(this.currentCheckpoint)[4]; distance = calculateDistance(Sboathud.boatData.x, Sboathud.boatData.z)) {
            double distanceLast = calculateDistance(Sboathud.boatData.xLast, Sboathud.boatData.zLast);
            double subtick = Math.min(Math.max((data.get(this.currentCheckpoint)[4] - distanceLast) / (distance - distanceLast), 0), 1);
            double checkpointTime = this.time - 0.05 + subtick * 0.05;
            
            if (this.currentCheckpoint == 0) {
                if (this.startTime != 0) {
                    this.lastLap = checkpointTime - this.startTime;
                    this.averageLap = (this.averageLap * this.lapCount + this.lastLap) / ++this.lapCount;
                    if (this.bestLap == 0 || this.lastLap < this.bestLap) this.bestLap = this.lastLap;
                    if (Sboathud.config.lapTimeStats) HudRenderer.sendLaptimes();
                }
                this.startTime = checkpointTime;
            }

            this.timeDiff = checkpointTime - this.startTime - data.get(this.currentCheckpoint)[0];
            this.speedDiff = Sboathud.boatData.speedLast * subtick + Sboathud.boatData.speed * (1 - subtick) - data.get(this.currentCheckpoint)[1];

            this.currentCheckpoint++;
            if (this.currentCheckpoint == this.checkpointCount) {
                this.currentCheckpoint = 0;
            }
        }
        this.time += 0.05;
    }

    public double calculateDistance(double x, double z) {
        // Distance along direction of travel
        return x * data.get(this.currentCheckpoint)[2] + z * data.get(this.currentCheckpoint)[3];
    }
}
