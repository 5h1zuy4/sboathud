package shizuya.sboathud;

import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

import static shizuya.sboathud.Sboathud.getBoatData;
import static shizuya.sboathud.Sboathud.getDelta;

public class HudRenderer {

    private static final Identifier WIDGETS_TEXTURE = Identifier.of("sboathud","textures/widgets.png");
    private static final HudRenderer INSTANCE = new HudRenderer();
    private static final MinecraftClient CLIENT = MinecraftClient.getInstance();

    //                                                Pack   Mix  Blue
    private static final double[] MIN_V           = {   0d,   0d,  40d}; // Minimum display speed (m/s)
    private static final double[] MAX_V           = {  40d,  72d, 72.7}; // Maximum display speed (m/s)
    private static final double[] SCALE_V         = {  5.4,  3.0,  6.6}; // Pixels for 1 unit of speed (px*s/m) (BarWidth / (VMax - VMin))
    private static final double[] SCALE_V_COMPACT = {  3.6,  2.0,  4.4};

    private static int centreX;
    private static int centreY;
    private static int bottomY;
    private static int[] slotsX;
    private static int[] slotsCompactX;
    private static int[] slotsY;

    private static BoatData boatData;
    private static Delta delta;

    public static HudRenderer get() {
        return INSTANCE;
    }

    public void render(DrawContext context) {
        centreX = CLIENT.getWindow().getScaledWidth() / 2;
        centreY = CLIENT.getWindow().getScaledHeight() / 2;
        bottomY = CLIENT.getWindow().getScaledHeight() - Sboathud.config.yOffset + 6;
        slotsX = new int[]{centreX - 104, centreX - 14, centreX + 14, centreX + 104};
        slotsCompactX = new int[]{centreX - 68, centreX, centreX + 68};
        slotsY = new int[]{bottomY - 14, bottomY - 4};
        boatData = getBoatData();
        delta = getDelta();
        RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
        RenderSystem.setShaderTexture(0, WIDGETS_TEXTURE);
        RenderSystem.enableBlend();
        RenderSystem.defaultBlendFunc();
        drawBackground(context);
        drawSpeedBar(context);
        if (Sboathud.config.extendedHud) {
            drawGMeter(context);
            drawThrottleTrace(context);
            drawSteeringTrace(context);
        } else {
            drawKeyInputs(context);
        }
        displayText(context);
        RenderSystem.disableBlend();
    }

    private static void drawBackground(DrawContext context) {
        context.drawTexture(WIDGETS_TEXTURE, centreX - getTextureWidth() / 2, bottomY - 20, 0, getTextureOffset(), getTextureWidth(), 26);
        // if (Sboathud.config.lapTimeStats) context.drawTexture(WIDGETS_TEXTURE, 0, centreY - 21, 0, 112, 70, 42);
        if (Sboathud.config.cringeCount) context.drawTexture(WIDGETS_TEXTURE, 0, centreY - 11, 0, 154, 38, 22);
    }

    private static void drawSpeedBar(DrawContext context) {
        context.drawTexture(WIDGETS_TEXTURE, centreX - getTextureWidth() / 2, bottomY - 20, 0, 26 + getTextureOffset() + Sboathud.config.barType.ordinal() * 10, getTextureWidth(), 5);
        if (boatData.speed < MIN_V[Sboathud.config.barType.ordinal()]) return;
        if (boatData.speed > MAX_V[Sboathud.config.barType.ordinal()]) {
            if ((CLIENT.world.getTime() & 2) != 0) { // Flash
                context.drawTexture(WIDGETS_TEXTURE, centreX - getTextureWidth() / 2, bottomY - 20, 0, 26 + getTextureOffset() + Sboathud.config.barType.ordinal() * 10 + 5, getTextureWidth(), 5);
            }
            return;
        }
        context.drawTexture(WIDGETS_TEXTURE, centreX - getTextureWidth() / 2, bottomY - 20, 0, 26 + getTextureOffset() + Sboathud.config.barType.ordinal() * 10 + 5, getBarLength() , 5);
    }

    private static int getTextureOffset() {
        return Sboathud.config.extendedHud ? 0 : 56;
    }

    private static int getTextureWidth() {
        return Sboathud.config.extendedHud ? 218 : 146;
    }

    private static int getBarLength() {
        return (int) ((boatData.speed - MIN_V[Sboathud.config.barType.ordinal()]) * (Sboathud.config.extendedHud ? SCALE_V[Sboathud.config.barType.ordinal()] : SCALE_V_COMPACT[Sboathud.config.barType.ordinal()]) + 1.5);
    }

    private static void drawGMeter(DrawContext context) {
        context.drawTexture(WIDGETS_TEXTURE, centreX - 9, bottomY - 14, 218, 0, 18, 18);
        context.drawTexture(WIDGETS_TEXTURE, centreX - 1 + getGMeterPosition(boatData.latAcc), bottomY - 6 - getGMeterPosition(boatData.lonAcc), getGMeterColour(), 0, 2, 2);
    }

    private static int getGMeterPosition(double g) {
        return Math.min(Math.max((int) (g / 2.5), -8), 8);
    }

    private static int getGMeterColour() {
        return Math.abs(boatData.lonAcc) > 22.5 || Math.abs(boatData.latAcc) > 22.5 ? 238 : 236;
    }

    private static void drawThrottleTrace(DrawContext context) {
        int i = 0;
        for (double throttle : boatData.throttleTrace) {
            context.drawTexture(WIDGETS_TEXTURE, slotsX[1] - 40 + i, slotsY[0] + 4 + getTracePosition(throttle), getThrottleColour(throttle), 0, 1, 1);
            i++;
        }
    }

    private static void drawSteeringTrace(DrawContext context) {
        int i = 0;
        for (double steering : boatData.steeringTrace) {
            context.drawTexture(WIDGETS_TEXTURE, slotsX[2] + i, slotsY[0] + 4 - getTracePosition(steering), 242, 0, 1, 1);
            i++;
        }
    }

    private static int getThrottleColour(double throttle) {
        return throttle > 0 ? 240 : throttle < 0 ? 241 : 242;
    }

    private static int getTracePosition(double value) {
        return ((int) Math.signum(value)) * -4;
    }

    private static void drawKeyInputs(DrawContext context) {
        context.drawTexture(WIDGETS_TEXTURE, slotsCompactX[1] - 27, slotsY[1], 146, 56, 42, 9);
        Boolean[] inputs = {CLIENT.options.leftKey.isPressed(), 
                            CLIENT.options.backKey.isPressed(), 
                            CLIENT.options.forwardKey.isPressed(), 
                            CLIENT.options.rightKey.isPressed()};
        for (int i = 0; i < 4; i++) {
            if (inputs[i]) context.drawTexture(WIDGETS_TEXTURE, slotsCompactX[1] - 27 + 11 * i, slotsY[1], 146 + 11 * i, 65, 9, 9);
        }
    }

    private static void displayText (DrawContext context) {
        if (Sboathud.config.extendedHud) {
            drawTextAlign(context, formatSpeed(boatData.speed), slotsX[0], slotsY[0], Alignment.LEFT);
            drawTextAlign(context, formatSlipAngle(boatData.slipAngle), slotsX[3], slotsY[0], Alignment.RIGHT);
            drawTextAlign(context, formatPing(boatData.ping), slotsX[2], slotsY[1], Alignment.LEFT);
            drawTextAlign(context, formatFps(boatData.fps), slotsX[3], slotsY[1], Alignment.RIGHT);
            if (Sboathud.config.deltaEnabled && delta != null) {
                drawTextAlign(context, formatTimeDiff(delta.timeDiff), slotsX[0], slotsY[1], Alignment.LEFT);
                drawTextAlign(context, formatSpeedDiff(delta.speedDiff), slotsX[1], slotsY[1], Alignment.RIGHT);
            } else {
                drawTextAlign(context, formatAccelerationLong(boatData.lonAcc, boatData.latAcc), slotsX[1] - 1, slotsY[1], Alignment.RIGHT);
            }
        } else {
            drawTextAlign(context, formatSpeed(boatData.speed), slotsCompactX[0], slotsY[0], Alignment.LEFT);
            drawTextAlign(context, formatSlipAngle(boatData.slipAngle), slotsCompactX[2], slotsY[0], Alignment.RIGHT);
            drawTextAlign(context, formatPing(boatData.ping), slotsCompactX[0], slotsY[1], Alignment.LEFT);
            drawTextAlign(context, formatFps(boatData.fps), slotsCompactX[2], slotsY[1], Alignment.RIGHT);
            if (Sboathud.config.deltaEnabled && delta != null) {
                drawTextAlign(context, formatTimeDiff(delta.timeDiff), slotsCompactX[1] + 6, slotsY[0], Alignment.CENTRE);
            } else {
                drawTextAlign(context, formatAcceleration(boatData.lonAcc), slotsCompactX[1] + 6, slotsY[0], Alignment.CENTRE);
            }
        }
        // if (Sboathud.config.lapTimeStats) {
        //     drawTextAlign(context, "Laps:", 2, centreY - 21 + 2, Alignment.LEFT);
        //     drawTextAlign(context, "Last:", 2, centreY - 21 + 12, Alignment.LEFT);
        //     drawTextAlign(context, "Aver:", 2, centreY - 21 + 22, Alignment.LEFT);
        //     drawTextAlign(context, "Best:", 2, centreY - 21 + 32, Alignment.LEFT);
        //     drawTextAlign(context, String.format("%d", DELTA.lapCount), 70 - 2, centreY - 21 + 2, Alignment.RIGHT);
        //     drawTextAlign(context, formatLapTime(DELTA.lastLap), 70 - 2, centreY - 21 + 12, Alignment.RIGHT);
        //     drawTextAlign(context, formatLapTime(DELTA.averageLap), 70 - 2, centreY - 21 + 22, Alignment.RIGHT);
        //     drawTextAlign(context, formatLapTime(DELTA.bestLap), 70 - 2, centreY - 21 + 32, Alignment.RIGHT);
        // }
        if (Sboathud.config.cringeCount) {
            drawTextAlign(context, "WT:", 2, centreY - 11 + 2, Alignment.LEFT);
            drawTextAlign(context, "BS:", 2, centreY - 11 + 12, Alignment.LEFT);
            drawTextAlign(context, String.format("%d", boatData.walltaps), 38 - 2, centreY - 11 + 2, Alignment.RIGHT);
            drawTextAlign(context, String.format("%d", boatData.blockstops), 38 - 2, centreY - 11 + 12, Alignment.RIGHT);
        }
    }

    private static void drawTextAlign(DrawContext context, String text, int x, int y, Alignment align) {
        context.drawTextWithShadow(CLIENT.textRenderer, text, x - CLIENT.textRenderer.getWidth(text) * align.ordinal() / 2, y, 0xFFFFFF);
    }

    private enum Alignment {
        LEFT, CENTRE, RIGHT
    }

    public static void sendLaptimes() {
        String colour = "";
        if (delta.lastLap <= delta.bestLap) {
            colour = "§d";
        } else if (delta.lastLap < delta.averageLap) {
            colour = "§a";
        } else if (delta.lastLap > delta.averageLap) {
            colour = "§c";
        }
        String msg = String.format("Lap %d: %s%s§r (avg: %s, best: %s)", delta.lapCount, colour, 
            formatLapTime(delta.lastLap), formatLapTime(delta.averageLap), formatLapTime(delta.bestLap));
        CLIENT.player.sendMessage(Text.literal(msg));
    }

    private static String formatSpeed(double speed) {
        double multiplier = 1.0;
        String unit = " m/s";
        switch (Sboathud.config.speedUnit) {
        case MS:
        default:
            break;
        case KMH:
            multiplier = 3.6;
            unit = " km/h";
            break;
        case MPH:
            multiplier = 2.236936;
            unit = " mph";
            break;
        }
        return threeDigits(speed * multiplier, false) + unit;
    }

    private static String formatSlipAngle(double slipAngle) {
        return threeDigits(Math.abs(slipAngle), false) + " °";
    }

    private static String formatAccelerationLong(double lonAcc, double latAcc) {
        double multiplier = 1.0;
        String unit = " m/s²";
        switch (Sboathud.config.accelerationUnit) {
        case MSS:
        default:
            break;
        case G:
            multiplier = 1.0 / 9.80665;
            unit = " g";
            break;
        }
        return threeDigits(lonAcc * multiplier, true) + " / " + threeDigits(Math.abs(latAcc * multiplier), false) + unit;
    }

    private static String formatAcceleration(double acceleration) {
        double multiplier = 1.0;
        String unit = " m/s²";
        switch (Sboathud.config.accelerationUnit) {
        case MSS:
        default:
            break;
        case G:
            multiplier = 1.0 / 9.80665;
            unit = " g";
            break;
        }
        return threeDigits(acceleration * multiplier, true) + unit;
    }

    private static String formatTimeDiff(double timeDiff) {
        String colour = timeDiff > 0.025 ? "§c" : timeDiff < -0.025 ? "§a" : "§f";
        return colour + threeDigits(timeDiff, true) + "s";
    }

    private static String formatSpeedDiff(double speedDiff) {
        String colour = speedDiff < -0.4 ? "§c" : speedDiff > 0.4 ? "§a" : "§f";
        double multiplier = 1.0;
        String unit = " m/s";
        switch (Sboathud.config.speedUnit) {
        case MS:
        default:
            break;
        case KMH:
            multiplier = 3.6;
            unit = " km/h";
            break;
        case MPH:
            multiplier = 2.236936;
            unit = " mph";
            break;
        }
        return colour + threeDigits(speedDiff * multiplier, true) + unit;
    }

    private static String formatLapTime(double time) {
        switch (Sboathud.config.lapTimeUnit) {
        case MS:
        default:
            return String.format("%d:%05.2f", (int) time / 60, time % 60);
        case S:
            return String.format("%06.2f", time);
        }
    }

    private static String formatPing(int ping) {
        String colour = ping > 500 ? "§c" : ping < 50 ? "§a" : "§f";
        return colour + String.format("%03d §fms", ping);
    }

    private static String formatFps(int fps) {
        String colour = fps < 60 ? "§c" : fps >= 240 ? "§a" : "§f";
        return colour + String.format("%03d §ffps", fps);
    }

    private static String threeDigits(double value, boolean includeSign) {
        String format;
        if (includeSign) {
            format =  Math.abs(value) >= 99.95 ? "%+.0f" : Math.abs(value) >= 9.95 ? "%+.1f" : "%+.2f";
        } else {
            format = Math.abs(value) >= 99.95 ? "%.0f" : Math.abs(value) >= 9.95 ? "%.1f" : "%.2f";
        }
        return String.format(format, value);
    }
}
