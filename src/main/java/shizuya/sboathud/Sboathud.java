package shizuya.sboathud;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.vehicle.BoatEntity;

public class Sboathud implements ClientModInitializer {

    public static BoatData boatData;
    public static MinecraftClient client;
    public static boolean ridingBoat;
    public static ModConfig config;

    @Override
    public void onInitializeClient() {
        client = MinecraftClient.getInstance();
        AutoConfig.register(ModConfig.class, GsonConfigSerializer::new);
        config = AutoConfig.getConfigHolder(ModConfig.class).getConfig();
        ClientTickEvents.END_WORLD_TICK.register(clientWorld -> {
            if (client.player == null) return;

            if (client.player.getVehicle() instanceof BoatEntity) {
                boatData.update();
            } else {
                ridingBoat = false;
            }
        });
    }

    public static BoatData getBoatData() {
        return boatData;
    }

    public static Delta getDelta() {
        return boatData.delta;
    }
}
